#!/usr/bin/env python3
# -*- coding: utf-8 -*-

##
# =============================================================================
#
# To run unit test :
#    python -m unittest discover utest
#
# License: GPL-2+
#   This package is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This package is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   If you need the GNU General Public License write to:
#     Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston,
#     MA 02110-1301, USA.
#
# (c) 2017, Pascal Rapaz (pascal.rapaz@rapazp.ch)
# =============================================================================
##
import unittest
import os
import sys

from glob import glob

import notestocsv

class TestCases(unittest.TestCase):

  @classmethod
  def setUpClass(self):
    self.maxDiff = None
    self.filepath = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'docs')
  #endDef

  @classmethod
  def tearDownClass(self):
    # delete all generated files
    for f in glob(os.path.join(self.filepath, '*.csv')):
      os.unlink(f)
    #endFor
  #endDef

  def test_notes_in_tenth(self):
    new = notestocsv.extract_notes(os.path.join(self.filepath, 'lnotes.pdf'), True)
    f = open(os.path.join(self.filepath, 'lnotes_tenth.ref'), "r")
    ref = f.read()
    f.close()

    self.assertMultiLineEqual(new, ref)
  #endDef

  def test_notes(self):
    new = notestocsv.extract_notes(os.path.join(self.filepath, 'lnotes.pdf'))
    f = open(os.path.join(self.filepath, 'lnotes.ref'), "r")
    ref = f.read()
    f.close()

    self.assertMultiLineEqual(new, ref)
  #endDef

  def test_bulletin_part(self):
    new = notestocsv.extract_bulletin(os.path.join(self.filepath, 'bpart.pdf'))
    f = open(os.path.join(self.filepath, 'bpart.ref'), "r")
    ref = f.read()
    f.close()

    self.assertMultiLineEqual(new, ref)
  #endDef

  def test_bulletin_sem1(self):
    new = notestocsv.extract_bulletin(os.path.join(self.filepath, 'bsem1.pdf'))
    f = open(os.path.join(self.filepath, 'bsem1.ref'), "r")
    ref = f.read()
    f.close()

    self.assertMultiLineEqual(new, ref)
  #endDef

  def test_bulletin_sem2(self):
    new = notestocsv.extract_bulletin(os.path.join(self.filepath, 'bsem2.pdf'))
    f = open(os.path.join(self.filepath, 'bsem2.ref'), "r")
    ref = f.read()
    f.close()

    self.assertMultiLineEqual(new, ref)
  #endDef
#endClass

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Main
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
if __name__ == '__main__':
  unittest.main()
#endIf
