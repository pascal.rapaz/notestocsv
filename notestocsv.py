#!/usr/bin/env python3
# -*- coding: utf-8 -*-

##
# =============================================================================
#
# Retrieve only evaluations ratings from pdf files
#
# Author : Pascal Rapaz
# Date   : 11.05.2017 Creation
#          21.12.2018 Add Windows support
# Version: 1.0.9
#
# License: GPL-2+
#   This package is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This package is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   If you need the GNU General Public License write to:
#     Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston,
#     MA 02110-1301, USA.
#
# (c) 2017-2018, Pascal Rapaz (pascal.rapaz@rapazp.ch)
# =============================================================================
##

import os
import re
import sys

import argparse
import textwrap

import logging

from subprocess import Popen, PIPE
from sys import platform

if sys.version_info[0] < 3:
  raise Exception("Must be using Python 3")
#endIf

VERSION = "1.0.8"
VERSION_MSG = "Application     : %s" % __file__ +\
              "\nRelease version : %s" % VERSION +\
              "\n(C) 2017-2018 by Pascal Rapaz (pascal.rapaz@gmail.com)"

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Main functions
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def extract_matu(path):
  text_file = convert_pdf_to_text(path)
  lst_matu = parse_matu_list(text_file)
  save_text(lst_matu, path)

  return lst_matu
#endDef

def extract_notes(path, tenth=False):
  text_file = convert_pdf_to_text(path)
  lst_notes = parse_notes_list(text_file)
  final_list = extract_from_notes(lst_notes, tenth)
  notes_csv = convert_csv(final_list)
  save_to_csv(notes_csv, path)
  logging.info(notes_csv)

  return notes_csv
#endDef

def extract_bulletin(path):
  text_file = convert_pdf_to_text(path)
  lst_bulletin = parse_bulletin_list(text_file)
  final_list = extract_from_bulletin(lst_bulletin)
  notes_csv = convert_csv(final_list)
  save_to_csv(notes_csv, path)
  logging.info(notes_csv)

  return notes_csv
#endDef

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Manage matu
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_matu_list(lst_moyennes):
  """
  Parse note list in a matrix

  @return : A matrix with all notes
  """

  tbl = []
  crt_student = ""
  crt_classe = ""
  cpt = -1

  # Extract lines with "moyennes"
  for l in lst_moyennes.splitlines():

    # add new line if not the same student
    if(l.find("Liste") > -1):
      tmp = l.rsplit(' ', 2)
      tmp = tmp[-1] + " " + tmp[-2]
      if(tmp != crt_student):
        tbl.append([tmp])
        crt_student = tmp
        cpt += 1
      #endIf
    #endIf

    # retrieve current crt_classe
    if(l.find("Classe:") > -1):
      crt_classe = l
    #endIf

    # extract only if part of matu
    if("Classe: MPTASV" in crt_classe):

      # Keep line if :
      #  - there is no space at the begin
      #  - there is a number at the end
      if(not re.search(r'^ ', l)
         and
         (re.search(r'\.\d{2}$', l) or
          re.search(r'\.\d{1}$', l))):

        sp = l.rsplit(' ', 2)
        ct = [""] * 5
        ct[4] = sp[2]
        ct[3] = sp[1]

        if(re.search(r'^\d{2}.\d{2}.\d{4}', l)):
          # sp[0] = " ".join(" ".join(sp[0].split()).split()[:-2])
          lgn = " ".join(sp[0].split()).split()
          ct[0] = lgn[0]

          # exam title
          if(lgn[-1] == "x"):
            ct[1] = " ".join(lgn[1:-3])
          else:
            ct[1] = " ".join(lgn[1:-2])
          #endIf

          ct[2] = lgn[-2]
        else:
          ct[0] = sp[0].strip()
          ct[3] = sp[1][1:-1]
        #endIf

        tbl[cpt].append(ct)
      #endIf
    #endIf
  #endFor

  logging.debug(tbl)
  return tbl
#endDef

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Manage notes
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_notes_list(lst_moyennes):
  """
  Parse note list in a matrix

  @return : A matrix with all notes
  """

  tbl = []
  crt_student = ""
  cpt = -1

  # Extract lines with "moyennes"
  for l in lst_moyennes.splitlines():

    # add new line if not the same student
    if(l.find("Liste") > -1):
      tmp = l.rsplit(' ', 2)
      tmp = tmp[-1] + " " + tmp[-2]
      if(tmp != crt_student):
        tbl.append([tmp])
        crt_student = tmp
        cpt += 1
      #endIf
    #endIf

    # Keep line if :
    #  - there is no date or space at the begin
    #  - there is a number at the end
    if((not re.search(r'^\d{2}.\d{2}.\d{4}', l) and
        not re.search(r'^ ', l))
       and
       (re.search(r'\.\d{2}$', l) or
        re.search(r'\.\d{1}$', l))):

      sp = l.rsplit(' ', 2)
      sp[0] = sp[0].strip()
      sp[1] = sp[1][1:-1]
      tbl[cpt].append(sp)
    #endIf
  #endFor

  logging.debug(tbl)
  return tbl
#endDef

def extract_from_notes(lst_notes, tenth=False):
  """
  Final extraction from list

  @return a list of notes
  """

  size, rowid = get_size_of_longest_line(lst_notes)
  tbl = init_table(size, rowid, lst_notes)

  # definie the row with note
  note_idx = 2
  if tenth:
    note_idx = 1
  #endIf

  # insert line with students notes
  for row in range(len(lst_notes)):
    # retrieve name
    tbl[row + 1][0] = lst_notes[row][0]

    for colInRow in range(1, len(lst_notes[row])):
      id = tbl[0].index(lst_notes[row][colInRow][0])
      tbl[row + 1][id] = lst_notes[row][colInRow][note_idx]
    #endFor
  #endFor

  delete_empty_column(tbl)

  logging.debug(tbl)
  return tbl
#endDef

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Manage bulletin
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_bulletin_list(lst_moyennes):
  """
  Parse bulletin list in a matrix (without "moyenne annuelle du groupe")

  @return : A matrix with all notes
  """

  tbl = []
  cpt = -1
  pos = 0
  crt_student = ""

  # Extract lines with "moyennes"
  for l in lst_moyennes.splitlines():

    if(l.find("Sem") > -1):
      pos = l.find("Sem")
    #endIf

    # add new line if not the same student
    if(l.find("Valais") > -1):
      tmp = l.split('  ', 1)[0]
      if(tmp != crt_student):
        tbl.append([tmp])
        crt_student = tmp
        cpt += 1
      #endIf
    #endIf

    # Keep line if :
    #  - there is a decimal number at the end
    if(re.search(r'\.\d{1}$', l)):
      # ignore lines
      if(l.find("Absences") > -1 or
         l.find("semestrielle") > -1):

        continue
      #endIf

      content = []
      if(l.find("annuelle") > -1):
        content.append(l[:pos-5].strip())
        l = l[pos-5:]
      else:
        content.append(l[:pos].strip())
        l = l[pos:]
      #endIf

      while(l):
        content.append(l[:10].strip())
        l = l[10:]
      #endWhile

      tbl[cpt].append(content)
    #endIf
  #endFor

  logging.debug(tbl)
  return tbl
#endDef

def extract_from_bulletin(lst_notes):
  """
  Final extraction from list, keep only last "semestre"

  @return a list of notes
  """

  size, rowid = get_size_of_longest_line(lst_notes)
  tbl = init_table(size, rowid, lst_notes)

  # retrieve column used to store last "semestre"
  col_last_semestre = 0
  for i in range(1, len(lst_notes[rowid])):
    if (len(lst_notes[rowid][i]) > col_last_semestre):
      col_last_semestre = len(lst_notes[rowid][i])
    #endIf
  #endFor

  col_last_semestre -= 1
  for row in range(len(lst_notes)):
    moyenne_cpt = 0
    moyenne_annuelle_cpt = 0

    # retrieve name
    tbl[row + 1][0] = lst_notes[row][0]

    # retrieve "notes"
    for colInRow in range(1, len(lst_notes[row])):

      if (lst_notes[row][colInRow][0] == "Moyenne"):
        moyenne_cpt += 1

        id = -1
        for i in range(moyenne_cpt):
          id = tbl[0].index(lst_notes[row][colInRow][0], id + 1)
        #endFor

      elif (lst_notes[row][colInRow][0] == "Moyenne annuelle du groupe"):
        moyenne_annuelle_cpt += 1

        id = -1
        for i in range(moyenne_annuelle_cpt):
          id = tbl[0].index(lst_notes[row][colInRow][0], id + 1)
        #endFor

      else:
        id = tbl[0].index(lst_notes[row][colInRow][0])
      #endIf

      try:
        tbl[row + 1][id] = lst_notes[row][colInRow][col_last_semestre]
      except IndexError:
        continue
      #endTry
    #endFor
  #endFor

  delete_empty_column(tbl)

  logging.debug(tbl)
  return tbl
#endDef

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Helper methods
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def execute(command, exit=True):
  """
  Execute synchronus command.

  @param command: Command bash to launch
  @param exit: Exit if there is an execution error (Default: True)

  @return: A tuple 'stdout, stderr' there contains execution logs
  """

  stdout = None
  stderr = None

  p = Popen(command, bufsize=16000, stdout=PIPE, stderr=PIPE, shell=True, close_fds=True, universal_newlines=True)
  stdout, stderr = p.communicate()

  if (stdout):
    logging.debug('BASH execute : (%s, %s)\n%s' % (os.getcwd(), command, stdout))
  #endIf

  if (stderr):
    logging.error('BASH execute : (%s, %s)\n%s' % (os.getcwd(), command, stderr))
  #endIf

  if (0  != p.returncode and
    exit):
    sys.exit(p.returncode)
  #endIf

  return stdout, stderr
#endDef

def convert_pdf_to_text(filename):
  """
  Use command line to convert pdf to text

  @return : A text converted pdf
  """

  stdout = None
 
  if platform == "linux":
    stdout, stderr = execute("pdftotext -layout '" + filename + "' -")
  elif platform == "win32":
    stdout, stderr = execute("pdftotext -simple " + filename + " -")
  else:
    logging.error("Unknow platform : " + platform)
  #endIf

  return stdout
#endDef

def convert_csv(matrix):
  """
  Convert a matrix to csv

  @return: csv converted matrix
  """
  ret = ""
  for row in matrix:
    ret += '\t'.join(row)
    ret += '\n'
  #endFor

  logging.debug(ret[:-1])
  return ret[:-1]
#endDef

def save_to_csv(cvs_content, orig_filename):
  """
  Create a csv file in the same folder than the original file

  @param cvs_content: data to save in the csv file
  @param orig_filename: full path of the original file (including filename)
  """

  dir, file = os.path.split(orig_filename)
  output = os.path.splitext(file)[0] + ".csv"
  output_fpath = os.path.join(dir, output)

  f = open(output_fpath, "w")
  f.write(cvs_content)
  f.close()
#endDef

def save_text(data, orig_filename):
  """
  Create a csv file in the same folder than the original file

  @param data: list to save in text file
  @param orig_filename: full path of the original file (including filename)
  """

  dir, file = os.path.split(orig_filename)
  output = os.path.splitext(file)[0] + ".txt"
  output_fpath = os.path.join(dir, output)

  f = open(output_fpath, "w")

  for i in data:
    for j in i:
      if(isinstance(j, list)):
        f.write(",".join(j) + "\n")
      else:
        f.write(j + "\n")
      #endIf
    #endFor
  #endFor

  f.close()
#endDef

def get_size_of_longest_line(matrix):
  """
  Retrieve size of the longuest line of a matrix

  @param matrix: the matrix to analyse
  @return a tuple with size and rowid of the longest line of a matrix
  """
  stage = False
  size = 0
  rowid = 0
  for i in range(len(matrix)):
    if (len(matrix[i]) > size):
      size = len(matrix[i])
      rowid = i

      # workaround to add "Stage - Période x"
      for j in range(len(matrix[i])):
        if "Stage" in matrix[i][j][0] and not stage:
          size += 1
          stage = True
        #endIf 
      #endFor
      # end workaround
    #endIf
  #endFor

  
  # if ():
  #   size += 1
  # #endIf


  return size, rowid
#endDef

def init_table(size, rowid, matrix):
  """
  Prepare a table for student "notes" (one row by student)

  @param size: size of the longest line
  @param rowid: row id of the longest line
  @param matrix: the matrix to analyse

  @return an empty table with header as first row
  """

  tbl = [["" for i in range(size)] for j in range(len(matrix) + 1)]

  # workaround to add "Stage - Période x"
  isStage = False
  stage = [False, False, False]

  for i in range(len(matrix[rowid])):
    if "Stage" in matrix[rowid][i][0]:
      isStage = True

      if ("1" in matrix[rowid][i][0]):
        stage[0] = True
      elif ("2" in matrix[rowid][i][0]):
        stage[1] = True
      elif ("3" in matrix[rowid][i][0]):
        stage[2] = True
      #endIf 
    #endIf
  #endFor

  if isStage:
    size -= 1
  #endIf
  # end workaround

  tbl[0][0] = "Nom Prénom"

  for i in range(1, size):
    tbl[0][i] = matrix[rowid][i][0]
  #endFor
  logging.info( tbl[0][size - 1])
  if isStage:
    for i in range(len(stage)):
      if stage[i] is False:
        tbl[0][size] = "Stage - Période " + str(i + 1)
        logging.info( tbl[0][size])
  return tbl
#endDef

def delete_empty_column(matrix):
  """
  Remove a column if there is no numbers

  @param matrix: the matrix to analyse

  @return a matrix that contains no empty columns
  """

  lst = [True for i in range(len(matrix[0]))]
  for i in range(1, len(matrix)):
    for j in range(len(matrix[i])):
      if matrix[i][j]:
        lst[j] = False
      #endIf
    #endFor
  #endFor

  for i in reversed(range(len(lst))):
    if lst[i]:
      for j in range(len(matrix)):
        del matrix[j][i]
      #endFor
    #endIf
  #endFor
#endDef

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Startup methods
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_args(args):
  """
  Retrieve command line arguments
  """

  parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                   description=textwrap.dedent("""
    Retrieve all evaluations from pdf files
    """))

  parser.add_argument("-b", "--bulletin",
                      action='store',
                      help="Bulletin filename")

  parser.add_argument("-m", "--matu",
                      action='store',
                      help="Extract matu ratings")

  parser.add_argument("-n", "--notes",
                      action='store',
                      help="Ratting list filename")

  parser.add_argument("-t", "--tenth",
                      action='store_true',
                      help="Ratings calculated in tenth")

  parser.add_argument("-v", "--verbose",
                      action='store_true',
                      help="Increase execution verbosity")

  parser.add_argument("-V", "--version",
                      action='version',
                      help='Display application version',
                      version=VERSION_MSG)

  return parser.parse_args(args)
#endDef

if __name__ == "__main__":
  args = parse_args(sys.argv[1:])

  if args.verbose:
    logging.basicConfig(level=logging.DEBUG)
  else:
    logging.basicConfig(level=logging.INFO)
  #endIf

  if args.matu:
    extract_matu(os.path.expanduser(args.matu))
  #endIf

  if args.notes:
    extract_notes(os.path.expanduser(args.notes), args.tenth)
  #endIf

  if args.bulletin:
    extract_bulletin(os.path.expanduser(args.bulletin))
  #endIf
#endIf
